package com.example;

import java.util.List;

/**
 * Created by guido on 05/12/16.
 *
 * Items and categories are actually the same base type, so it's easy to handle them and to modify the menu.
 * Categories can be nested inside other categories "without limitations".
 *
 * They are organised in a hierarchical structure: if an item contains children than it's a category
 * This is very quick when showing the menu: click on an item and show all of the children, no need to parse the whole
 * structure, only children
 *
 * For other usage, for example a basket, I would create a HashMap<Integer, MenuItem> for quick non-sequential access
 *
 * The structure contains two mechanism to handle the hierarchy: the children field and the parent field, but it
 * makes sense as they are used in different context.
 * Also the menu doesn't change - at least not within the time the user uses the app, so there is no risk of inconsistency
 *
 * The only overhead is perhaps on server side, that has to create this structure (most likely) from a relational DB
 *
 * PLEASE NOTE: As for portions support I would use a new item altogether, to avoid overcomplicating the structure
 * Data varying with portion is price and ingredients (possibly with no relationship),
 * description would probably be the same, name also could change if we included the portion.
 * As a matter of fact the menu in http://www.gbk.co.uk/menu#menu2 is only for browsing, the online menu for orders
 * at https://order.gbk.co.uk/  doesn't support portions.
 * If we had more complex and well defined requirement for portions support, then we can study a data structure that
 * could implement it.
 *
 */
public class MenuItem {

    // category or item id, this is useful for non-hierarchical reference: HashMap, basket, parent iD
    public int id;

    // category or item name
    public String name;

    /* this would be:
     - null if it's a first level category
     - id of the parent entry

     This will be needed to be able to move onto the parent item, for example moving up the hierarchy and
      then into a different category*/
    public Integer parentId;

    // true if it's a category, false otherwise
    // first level entries should probably be only categories
    public boolean isCategory;

    public List<MenuItem> children;

    // only for items
    public String description;
    public double price;

    // list of ingredients for this MenuItem
    public List<Ingredient> ingredients;



    // other details from GBK's menu
    public boolean rh;
    public boolean nuts;


    // Allergens info: this could come from the server for each MenuItem or could be associated with ingredients.
    // The latter wouldn't account for allergens that are not in the ingredients but could possibly be in the menu item

}
