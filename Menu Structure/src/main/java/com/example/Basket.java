package com.example;

import java.util.List;

/**
 * This class handles the basket and all operations related to it
 */
public class Basket {

    // list of items added to the basket
    public List<BasketItem> basketItems;
}
