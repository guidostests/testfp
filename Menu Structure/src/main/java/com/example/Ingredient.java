package com.example;

/**
 * Ingredients structure can be kept much more simple, as their usage is limited and simple.
 *
 */

public class Ingredient {
    public int id;
    public String name;
    // description
    // allergen information

    // most of the times it would be 1, however there could be some menu items that contain 2 ingredients
    // Anyway this will allow the request from bonus 4)
    public double quantity;

    public double price;
}
