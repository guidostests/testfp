package com.example;

import java.util.List;

/**
 * Created by guido on 05/12/16.
 */

public class BasketItem {
    // reference to the menu's MenuItem
    public MenuItem menuItem;

    // quantity, usually integers... but you never know
    public double quantity;

    // this will account for quantity and additional ingredients
    public double price;

    /*
    * this should by default be the same as menuItem, but ingredients can then be amended without
    * modifying the original MenuItem: when
    * adding ingr: price will be updated accordingly
    * removing ingr: most likely price won't change....
    * swapped ingr: I would send to the server the ingredient and server will suggest a list of swappable ingr.
    *               Some logic would have to be implemented on the server to limit what is swappable.
    *               GrilledTo level (medium rare well done etc) could be implemented through a swap, but again should be
    *               implemented accurately on the server
    * */
    public List<Ingredient> ingredients;

    // this will iterate through ingredients and sum the prices, multiply by quantity
    // perhaps add a tip or fees, or detract a voucher/promo code
    public double getTotalPrice(){
        return 0;
    }
}
