package com.flypay.flypaytest.androidcandidatetest.mvp.mainactivity;

import com.flypay.flypaytest.androidcandidatetest.model.Forecast;
import com.guidovezzoni.mvp.ExtendedContract;

/**
 * Created by guido on 10/12/16.
 */

public interface MainActivityContract
        extends ExtendedContract<Forecast, Long,
                        MainActivityContract.View, MainActivityContract.Presenter, MainActivityContract.Model> {

    interface View extends ExtendedContract.View<Forecast, Presenter> {

    }

    interface Presenter extends ExtendedContract.Presenter<View, Model> {
        void viewPressedLondon();

        void viewPressedNY();
    }

    interface Model extends ExtendedContract.Model<Long, Presenter> {

    }
}
