package com.flypay.flypaytest.androidcandidatetest.api;

import com.flypay.flypaytest.androidcandidatetest.model.Forecast;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by guido on 09/12/16.
 */

public interface OpenWeatherMapService {
    // sample: http://api.openweathermap.org/data/2.5/forecast?id=%d&APPID=b83c13b2ab60f7ec52b12d6fcf3f9f42
    @GET(Api.DATA_FORECAST)
    Call<Forecast> getDataForecast(@Query(Api.QUERY_PARAM_CITY_ID) long id,
                                   @Query(Api.QUERY_PARAM_APP_ID) String appId,
                                   @Query(Api.QUERY_PARAM_UNITS) String units);
}
