
package com.flypay.flypaytest.androidcandidatetest.model;


public class City {

    public Integer id;
    public String name;
    public Coord coord;
    public String country;
    public Integer population;
    public Sys sys;

}
