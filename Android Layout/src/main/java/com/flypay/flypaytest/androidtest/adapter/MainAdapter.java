package com.flypay.flypaytest.androidtest.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.flypay.flypaytest.androidtest.R;
import com.flypay.flypaytest.androidtest.viewholder.MainViewHolder;

import java.util.List;

/**
 * Created by guido on 06/12/16.
 */

public class MainAdapter extends RecyclerView.Adapter<MainViewHolder> {
    private List<String> mStrings;

    public MainAdapter(List<String> strings) {
        mStrings = strings;
    }

    @Override
    public MainViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item, parent, false);
        return new MainViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MainViewHolder holder, int position) {
        holder.mTextView.setText(mStrings.get(position));
    }

    @Override
    public int getItemCount() {
        return mStrings.size();
    }
}
