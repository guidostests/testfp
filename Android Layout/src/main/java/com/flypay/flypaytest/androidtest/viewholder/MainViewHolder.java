package com.flypay.flypaytest.androidtest.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.flypay.flypaytest.androidtest.R;

/**
 * Created by guido on 06/12/16.
 */

public class MainViewHolder extends RecyclerView.ViewHolder {
    public TextView mTextView;

    public MainViewHolder(View itemView) {
        super(itemView);

        mTextView = (TextView) itemView.findViewById(R.id.textView);
    }

}
