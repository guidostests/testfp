package com.flypay.flypaytest.androidtest;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.flypay.flypaytest.androidtest.adapter.MainAdapter;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private final static int LIST_SIZE = 100;
    private final static String IMAGE_URL = "http://lorempixel.com/640/640";

    private Snackbar mSnackbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.app_bar_layout);
        appBarLayout.setExpanded(false);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ImageView toolbar_imageView = (ImageView) findViewById(R.id.toolbar_image);
        Glide.with(this)
                .load(IMAGE_URL)
                .centerCrop()
                .into(toolbar_imageView);

        // create a list for the adapter
        List<String> list = new ArrayList<>(LIST_SIZE);
        for (int ii = 0; ii < LIST_SIZE; ii++) {
            list.add(getString(R.string.item_text) + ii);
        }

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.list_recyclerView);
        recyclerView.setAdapter(new MainAdapter(list));

        // set up fab
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.snackbar_fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mSnackbar == null || !mSnackbar.isShown()) {
                    mSnackbar = Snackbar.make(view, R.string.snackbar_text, Snackbar.LENGTH_SHORT)
                            .setAction(R.string.snackbar_action, new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {

                                        }
                                    }
                            );
                    mSnackbar.show();
                }
            }
        });

    }
}
